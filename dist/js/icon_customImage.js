
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.70850607, 37.60258850],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Наш адрес',
            balloonContent: 'Мы здесь'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: 'https://gitlab.com/baomastr/apcmed-prod/raw/6cddd9e188528f3fc130a2bb5ccdcd75623c0e55/images/mapicon.png',
            // Размеры метки.
            iconImageSize: [30, 48],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-3, -42]
        });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');
});